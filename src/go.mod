module bin/app

require (
	github.com/antchfx/htmlquery v1.0.0
	github.com/antchfx/xpath v0.0.0-20190319080838-ce1d48779e67 // indirect
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/likexian/whois-go v0.0.0-20190427154104-ec9a326dddb5
	golang.org/x/net v0.0.0-20190514140710-3ec191127204 // indirect
)
