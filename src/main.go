package main

import (
	"fmt"
	"time"
	"encoding/json"
	"net/http"
	"net/url"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"strings"
	"regexp"
	"github.com/likexian/whois-go"
	"github.com/antchfx/htmlquery"
)

type Endpoint struct {
	Address 	   string `json:"ipAddress"`
	SSL_Grade	   string `json:"grade"`
	GradeTrustIgnored  string `json:"gradeTrustIgnored"`
	Country            string
	Owner              string
}

type SSLres struct {
	Host		    string	 `json:"host"`
	Port		    int   	 `json:"port"`
	IsPublic	    bool	 `json:"isPublic"`
	Status		    string	 `json:"status"`
	StartTime	    int64	 `json:"startTime"`
	TestTime	    int64	 `json:"testTime"`
	Servers  	    []*Endpoint  `json:"endpoints"`
	Logo                string
	Servers_changed     bool
	SSL_grade           string
	Previous_ssl_grade  string
	Is_down             bool
}

func getIconUrl(url string) string{
	match, _ := regexp.MatchString("http(s?)://*", url)
	if !match {
		url = "http://"+url
	}
	doc, err := htmlquery.LoadURL(url)
	if err != nil {
		fmt.Printf("Icon HTTP request failed with error %s\n", err)
		panic(err)
	}

	var icon = "No icon found"
	for i, n := range htmlquery.Find(doc, "//link") {
		relattr := htmlquery.SelectAttr(n, "rel")
		if relattr == "shortcut icon" {
			fmt.Printf("%d %s %s\n", i, htmlquery.SelectAttr(n, "rel"), htmlquery.SelectAttr(n, "href"))
			icon = htmlquery.SelectAttr(n, "href")
			break
		}
	}
	return icon
}

func getEndpointInfo(endpoints []*Endpoint) []*Endpoint{
	for _, element := range endpoints {
		result, err := whois.Whois(element.Address)
		if err == nil {
			countryRe,_ := regexp.Compile(`Country:.*`)
			country := strings.Fields(countryRe.FindString(result))
			if len(country) == 0 {
				element.Country = "No country reported"
			}else{
				element.Country = country[1]
			}
			
			ownerRe,_ := regexp.Compile(`Organization:.*`)
			owner := strings.Fields(ownerRe.FindString(result))[1:]
			if len(owner) == 0 {
				element.Owner = "No owner reported"
			}else{
				element.Owner = strings.Join(owner," ")
			}
		}
	}
	return endpoints
}

func main() {
	r := chi.NewRouter()

	// Base middleware stack
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	// Set a timeout value on the request context (ctx)
	r.Use(middleware.Timeout(60 * time.Second))

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Server working properly"))
	})

	// RESTy routes for "url" resource
	r.Route("/scan", func(r chi.Router) {
		r.Get("/{url}", scanUrl)
		r.Get("/", scanUrl)
	})

	fmt.Println("Starting server on port :3000")
	http.ListenAndServe(":3000", r)
}

func scanUrl(w http.ResponseWriter, r *http.Request) {
	val := url.QueryEscape(r.FormValue("url"))
	if val != "" {
		response, err := http.Get("https://api.ssllabs.com/api/v3/analyze?host=​"+val)
		if err != nil {
			fmt.Printf("The HTTP request failed with error %s\n", err)
			panic(err)
		} else {
			// Fill the record with the data from the JSON
			var record SSLres
			
			// Use json.Decode for reading streams of JSON data
			if err := json.NewDecoder(response.Body).Decode(&record); err != nil {
				fmt.Println(err)
			}
			
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusCreated)

			record.Servers = getEndpointInfo(record.Servers)
			record.Logo = getIconUrl(val)
			json.NewEncoder(w).Encode(record)
		}
		
	} else {
		fmt.Fprintf(w, "URL parameter required.")
	}
}
