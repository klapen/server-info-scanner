# server-info-scanner

Endpoint to get servers information

# Run project

1. Open src/ folder
2. Install (Golang-dev)[https://hub.docker.com/r/zephinzer/golang-dev/]
3. Make start

# Run CockroachDB server

Install CockroachDB or use docker:

```sh
$ docker run -it --name cockroach --rm -p 26257:26257 -p 8080:8080 -v "${PWD}/sql:/cockroach/sql" cockroachdb/cockroach start --insecure
```

# Start database

Connect to the cockroach server and run the files db/sql/createAdminUser.sql and db/sql/createTables.sql.

Remember that you need a admin db user to run those scripts.