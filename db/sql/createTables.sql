USE ssldata;

CREATE TABLE IF NOT EXISTS hosts (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    url STRING UNIQUE,
    ssl_grade STRING,
    prev_ssl_grade STRING,
    servers_changed BOOL,
    last_request DATE DEFAULT now(),
    is_down BOOL,
    INDEX url_idx (url)
);


CREATE TABLE IF NOT EXISTS hosts (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    host_id UUID NOT NULL REFERENCES hosts (id),
    address STRING UNIQUE,
    ssl_grade STRING,
    country STRING,
    owner STRING,
    last_request DATE DEFAULT now(),
    INDEX adress_idx (address),
);
